#!/bin/sh

openssl genrsa -out privkey.pem 4096
openssl req -new -x509 -nodes -days 360 -key privkey.pem -out cert.pem

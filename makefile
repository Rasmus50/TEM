CC=gcc
CFLAGS_INC := -lpthread -lcrypto -lssl -lbsd
CFLAGS := -g -O3 -Wall $(CFLAGS_INC)
PREFIX = /usr/share

all: client server

client: client.c
	$(CC) -o tem client.c $(CFLAGS)

server: server.c
	$(CC) -o tem_server server.c $(CFLAGS)

.PHONY:	install
install: tem tem_server
	mkdir -pv $(DESTDIR)$(PREFIX)/bin
	cp tem $(DESTDIR)$(PREFIX)/bin/tem
	cp tem_server $(DESTDIR)$(PREFIX)/bin/tem_server
	cp doc/tem.1 $(PREFIX)/man/man1/tem.1
	gzip $(PREFIX)/man/man1/tem.1
	cp doc/tem_server.1 $(PREFIX)/man/man1/tem_server.1
	gzip $(PREFIX)/man/man1/tem_server.1

.PHONY: uninstall
uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/tem*
	rm -f $(PREFIX)/man/man1/tem*

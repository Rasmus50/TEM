#include <stdio.h>	//used for standard library functionality including memory management
#include <stdlib.h>
#include <unistd.h>

#include <sys/socket.h> // used for networking functionality
#include <bsd/string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <time.h>	//used when assembling messages

#include <openssl/crypto.h>
#include <openssl/ssl.h>	//crypto functionality
#include <openssl/err.h>

//arbitrary port number
#define PORT 7272

//colours for formatting
#define ANSI_COLOR_RED	"\x1b[31m"	
#define ANSI_COLOR_GREEN "\x1b[32m"


/* This program is the client that users interact with */
/* connects to server */

char save;	//used to determine whether user wants to save messages to text file

typedef pthread_t OT_THREAD_HANDLE;

SSL_CTX* initCTX() {

	/* function used to initialize SSL context */
	
	
	const SSL_METHOD *meth;
	SSL_CTX *ctx;
	
	//load cryptography algorithms 
	OpenSSL_add_all_algorithms();
	//load error messages for printing in case of errors
	SSL_load_error_strings();
	//setting method to be used with server. in this case "TLS_method" allows client and server to negotiate for most recent version of TLS
	meth = TLS_method();
	//generates a context using the method chosen
	ctx = SSL_CTX_new(meth);
	//if no context is generated, print errors and quit
	if (ctx == NULL) {
		ERR_print_errors_fp(stderr);
		abort();
	}
	return ctx;
}

void show_certs(SSL *ssl) {

	X509 *cert;
	char *line;
	
	//get authentication from server
	cert = SSL_get_peer_certificate(ssl);
	//if server has a certificate then print details of that certificate
	if(cert != NULL) {
		printf("server certificates:\n");
		line = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
		printf("subject: %s\n", line);
		//free memory used
		free(line);
		X509_free(cert);
	}
	else {
		printf("no certificate\n");
	}
}

int save_to_file(char *buffer) {

	/* this function takes the buffer either from received messages or sent messages and saves them to a file "tem_log.tem" */

	//save the buffer pointer to str	
	char *str = buffer; 	
	//pointer to log file
	FILE *fp;
			
	//opening log file to append	
	fp = fopen("tem_log.tem", "a");

	//check if pointer is null
	if(fp == NULL) {
		//return error to user and exit
		perror("pointer error ");
		exit(1);
	}
	
	//write the buffer to the file fp points to
	fprintf(fp, "%s", str);

	//close the file that fp points to
	fclose(fp);
	
	return 0;
}

void *receive(void *ssl) {

	/* a new thread to enable multiplexing between receiving and sending messages */

	char *buffer;
	struct account *user;
	int len;
	
	//allocate appropriate memory to buffer
	buffer = (char *)calloc(256, 1);	

	//while the length of the message received from server is more than 0
	while((len = SSL_read(ssl, buffer, 256)) > 0) {	
		//output buffer to standard output
		fputs(buffer, stdout);
		//reset buffer to null
		memset(buffer, '\0', sizeof(&buffer));
		//print a newline for formatting purposes 
		printf("\n");
		}		
	//exit recieve thread
	pthread_exit(NULL);
}

int message_interface(SSL *ssl) {

	/* function to allow user to enter input and send it to server */

	char *buffer;
	char *contents;
	time_t ltime;
	struct tm *info;
	char time_buffer[20];
	char *newline;
	pthread_t receive_t;
	
	//allocate appropriate amount of memory to both buffer and contents
	buffer = (char *)calloc(256, 1);
	contents = (char *)calloc(200, 1);
	
	//if the thread is not successfully created
	if(pthread_create(&receive_t, NULL, receive, (void *)ssl) != 0) {
		//print error message
		perror("failed to create thread ");
	}
	//if the thread is successfully created
	else {
		printf("would you like to save messages to a local file?\t");
		//receives input from user specifying whether or not they want to save messages
		while(fgets(buffer, 3, stdin) == NULL){
			if(strncmp(buffer, "y", 1) == 0) {
				save = 'y';
			}
			else {
				save = 'n';
			}
		}
		memset(buffer, '\0', 256);
		printf("Enter a message... : ");
		
		while(fgets(contents, 202, stdin) > 0) {	
			
			//set newline to the first occurance of \n in contents
			newline = strchr(contents, '\n');
			//set the value that newline points to to \0
			*newline = '\0';
			
			//call time function with appropriate variable
			time(&ltime);
			
			//set info to system time
			info = localtime(&ltime);
			
			//format info into string time_buffer
			strftime(time_buffer, 20, "%x - %T:%M", info);
			
			//assemble each piece of the message into a complete buffer
			strcat(buffer, contents);
			strcat(buffer, "\t\t\t");
			strcat(buffer, time_buffer);
			strcat(buffer, "\n");
			
			//send assembled message to server
			SSL_write(ssl, buffer, strlen(buffer));
			
			//print the message to local user
			printf(ANSI_COLOR_GREEN "%s", buffer);	
			
			//if the user has chosen to save messages
			if(save == 'y') {
				//call save_to_file function
				save_to_file(buffer);
			}
			
			//reset buffer
			memset(buffer, '\0', strlen(buffer));

			}
		}
	//wait for completion of recieve thread when finished
	pthread_join(receive_t, NULL);	
	//close ssl state
	SSL_free(ssl);
	
	return 0;	
}
	
SSL *server_connect(char dest_ip[17]) { 

	/* function to connect the client to the specified server */
	
	int client_socket;
	int choice = 0;	

	SSL_CTX *ctx;
	SSL *ssl;
	
	
	//initialises a 'sockaddr_in' structure called 'server_addr'
	struct sockaddr_in server_addr;

	ctx = initCTX();

	//creates the TCP client socket
	client_socket = socket(PF_INET, SOCK_STREAM, 0);	
	//overwrites any memory stored in 'server_addr' to null
	memset(&server_addr, '\0', sizeof(server_addr));
	// specifies family of addresses (IPv4)
	server_addr.sin_family=AF_INET;
	// specifies port the server will be using
	server_addr.sin_port=htons(PORT); 
	//specifies server IP address received from user
	server_addr.sin_addr.s_addr=inet_addr(dest_ip);

	//if the connect function returns -1 (fails)
	if (connect(client_socket, (struct sockaddr*)&server_addr, sizeof(server_addr)) == -1) {	
		//prints an appropriate error message
		perror("[-] error connecting to server ");	
		//return error to main function
		return -1;
	}

	printf("[+] successfully connected\n");

	ssl = SSL_new(ctx);

	SSL_set_fd(ssl, client_socket);

	if(SSL_connect(ssl) == -1) {
		ERR_print_errors_fp(stderr);
		exit(1);
	}

	printf("using %s encryption\n", SSL_get_cipher(ssl));
	show_certs(ssl);

	return ssl;

}

char *input_dest() {
	/* receives address to server */
	char *dest_ip;

	dest_ip = (char *)calloc(17,1);

	puts("please enter the destination IP address : ");
	while(fgets(dest_ip, 17, stdin) == NULL) {
		perror("could not read IP address ");
	}
	return dest_ip;
}

int main() {

	/* main function that takes address of a server to connect to */
	char *ip; 
	SSL *ssl;

	//clear terminal screen	
	if(system("clear") < 0)
		perror("error clearing terminal ");
	ip = input_dest();
	ssl = server_connect(ip);
	message_interface(ssl);
	
	return 0;
}

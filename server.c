#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>

#include <sys/socket.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <pthread.h>
#include <errno.h>
#include <signal.h>

#include <openssl/ssl.h>
#include <openssl/err.h>


#define PORT 7272
#define PENDMAX 3
#define MAXCON 30

struct account {
	char ip[INET_ADDRSTRLEN];
	int sockno;
	SSL *ssl;
	SSL_CTX *ctx;
};

struct account users[MAXCON];

int clients[MAXCON];
int n_of_cc = 0;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

SSL_CTX *init_serverCTX() {
	

	/* function to create the context that will will be used to generate the ssl instance */

	const SSL_METHOD *meth;
	SSL_CTX *ctx;
	
	//add all currect algorithms into program to allow negotiation with client on which to use
	OpenSSL_add_all_algorithms();
	
	//load error strings for printing SSL errors
	SSL_load_error_strings();
	
	//delcare the method for ssl, in this case "TLS_method" allows for negotiation between client and server
	meth = TLS_method();
		
	//generate context using method
	ctx = SSL_CTX_new(meth);
	
	//if no context is generated then print errors
	if(ctx == NULL) {
		ERR_print_errors_fp(stderr);
		abort();
	}
	return ctx;
}

void load_certificates(SSL_CTX *ctx, char *cert_file, char *key_file) {
	
	/* function for inspecting server certificates, program will not continue if server is not secured with certificate and key */
	
	//if server cannot find the certificte in active directory then print errors and exit program
	if(SSL_CTX_use_certificate_file(ctx, cert_file, SSL_FILETYPE_PEM) <= 0){
		ERR_print_errors_fp(stderr);
		printf("please generate certificate and private key as details in man page");
		abort();
	}
	//if server cannot find the private key in active directory then print the errors and exit program
	if (SSL_CTX_use_PrivateKey_file(ctx, key_file, SSL_FILETYPE_PEM) <= 0) {
		ERR_print_errors_fp(stderr);
		printf("please generate certificate and private key as details in man page");
		abort();
	}	
}

void show_certs(SSL *ssl) {

	/* print certificates of client if they have one, though not necessary for program to function */

	X509 *cert;
	char *line;
	
	//recieve certificate from client using the negotiated ssl instance
	cert = SSL_get_peer_certificate(ssl);
	
	//if certificate is present then print details
	if(cert != NULL) {
		printf("server certificates:\n");
		line = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
		printf("Server: %s\n", line);
		free(line);
		line = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0);
		printf("Client: %s\n", line);
		//free memory used for operation
		free(line);
		X509_free(cert);
	}else {
		printf("no certificates\n");
	}
}		

void forward(char *buffer, int current, SSL *ssl) {

	//lock thread
	pthread_mutex_lock(&mutex);
	//loop through array of conected sockets
	for(int i = 0; i < n_of_cc; i++) {
		//when the socket number isn't the same as the current connected client, forward message
		if(clients[i] != current) {
			//if send function fails print error
			if(SSL_write(users[i].ssl, buffer, strlen(buffer)) < 0) {
				perror("failed to send message");
				continue;
			
			}
		}
	}
	//unlock thread
	pthread_mutex_unlock(&mutex);	
}

void message_interface(struct account *acc) {

	/* thread to receive and forward messages */
	
	char *buffer;
	pthread_t forward_t;
	//reference pointer to structure acc so it can be used in function
	struct account current = *((struct account *)acc);

	//allocate memory to buffer
	buffer=(char *)malloc(256);
	//create thread for messaging interface and pass account structure to it
	pthread_create(&forward_t, NULL, (void *)message_interface, (void *)&current);
	//exit handler
	pthread_exit(NULL);
	//while thread receives message without error
	while((SSL_read(current.ssl, buffer, 256)) > 0) {
		//print buffer to screen
		printf("\n%s\n", buffer);
		//forward to other clients
		forward(buffer, current.sockno, current.ssl);
		//reset buffer
		memset(buffer, '\0', sizeof(*buffer));	
	}
	//notify server when a user disonnects
	printf("user on %s has disconnected\n", current.ip);
	//decrement number of connected clients (array of socket numbers)
	n_of_cc--;
	//exit thread
	pthread_exit(NULL);
}
	
void *handle(void *sock) {

	/* main menu system for sign in and account creation*/

	struct account user = *((struct account *)sock);
		
	//generate ssl object
	user.ssl = SSL_new(user.ctx);
	//'bind' ssl context to connected socket
	SSL_set_fd(user.ssl, user.sockno);
	
	//negotiate with client to create a TLS session
	if(SSL_accept(user.ssl) == -1) {
		//if fail then print error
		ERR_print_errors_fp(stderr);
	}
	else {
		//retreive client certificate(s) 
		show_certs(user.ssl);
	}

	users[n_of_cc] = user;
	pthread_mutex_unlock(&mutex);
	message_interface(&user);

	return 0;
}
int main() { 

	/* main function that accepts incoming connections from clients and creates a new thread to keep track of each one */

	 int sockfd, new_socket; //activity;
	 struct sockaddr_in server_addr, new_addr; 
	 pthread_t handle_t;
	 struct account user;
 	 socklen_t addr_size;  

	 //clear the terminal
	 system("clear");

	 //generate context and store in ctx
	 user.ctx = init_serverCTX();
	 

	 
	
	 //create socket and store in sockfd
	 sockfd = socket(PF_INET, SOCK_STREAM, 0);

	 //retrieves files used by ssl functions from active directory generated by users
	 //details in doc files
	 load_certificates(user.ctx, "cert.pem", "privkey.pem");
	

	
	 //set address information structure to null
	 memset(&server_addr, '\0', sizeof(server_addr));
	
	 //if return value of socket() call is an error value print error
	 if (sockfd == -1) {
		 perror("[-] error opening socket ");
	 }
	 else {
		 printf("[+] socket successfully opened\n");
	 }
	
	 //set address information 
	 //set address family to IPv4
	 server_addr.sin_family=AF_INET;
	 //set port to predefined PORT
	 server_addr.sin_port=htons(PORT);
	 //set address to addresses pointing to local machine
	 server_addr.sin_addr.s_addr= INADDR_ANY;
	
	 //bind socket to port 
	 if (bind(sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr)) == -1) {
		 //print error if bind() call returns error value
		 perror("[-] error binding socket to address");
	 }
	 else { 
		 puts("[+] socket successfully bound to port");
	 }
	
	 //listen on socket for incoming connections
	 if (listen(sockfd, PENDMAX) < 0) {
		 perror("[-] error listening ");
	 }
	 else {
 		 printf("[+] listening on port %i...\n", PORT);
	 }
			
	 /*monitoring multiple clients */
	
	 //infinite loop
	 while(1) {
		 //generate a new socket for each accepted connection
		 new_socket = accept(sockfd, (struct sockaddr*)&new_addr, &addr_size);
		 //exit program if return value for accept() call is error value
		 if(new_socket < 0) {
			 exit(1);
		 }
		 pthread_mutex_lock(&mutex);	 
		 //notify server when a user connects 
		 printf("user has connected on %s\n", inet_ntoa(new_addr.sin_addr));
		 //add new socket to socket user structure
		 user.sockno = new_socket;
		 //copy ip address to user structure 
		 strcpy(user.ip, inet_ntoa(new_addr.sin_addr));
		 //create new threat to handle multiple clients interacting with menu
		 pthread_create(&handle_t, NULL, handle, &user);
		 pthread_mutex_unlock(&mutex);
		 
	 }
	 return 0;
}

